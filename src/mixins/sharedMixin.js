import moment from "moment";
import 'moment/locale/pt-br';
moment.locale('pt-br')
// import format from "date-fns/format";
export const sharedMixin = {
    methods: {
        formatDateToScreen(date) {
            var moment_date = moment(date).format("DD/MM/YYYY");
            return moment_date != "Invalid date" ? moment_date : "";
          },
    },
    computed: {
        currentUserFirstName() {
            return this.$store.state.currentUser.firstName;
        },
        currentUserAdmin() {
            return this.$store.state.currentUser.admin;
        },
    }
}