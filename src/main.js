import Vue from 'vue'
import App from './App.vue'
import './quasar'

import store from "./store/store";

import Vuelidate from "vuelidate";

import VueRouter from "vue-router";
import { routes } from "./router/index.js";

import VueAxios from "vue-axios";
import axiosHttp from "./services/axios";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes
});

router.beforeEach( (to, from, next) => {
  if (to.meta.requiresAuth && !localStorage.getItem('repassa_auth') ) {
    next({
      path: '/login'
    })
    return
  }
  next()
})

Vue.use(VueAxios, axiosHttp);

Vue.use(Vuelidate)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  axiosHttp,
  store,
  router
}).$mount('#app')
