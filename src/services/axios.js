import axios from 'axios'

const API_URL = 'http://localhost:3000/api/'

const axiosHttp = axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
  withCredentials: true
})

// interceptor is used to update the csrf value at axios header
// axiosHttp.interceptors.request.use(config => {
//   const method = config.method.toUpperCase();
//   if(method !== 'OPTIONS' && method !== 'GET') {
//     config.headers = {
//       ...config.headers,
//       "X-CSRF-TOKEN": getCookie('csrf')
//     }
//   }
//   return config;
// }, 
// (error) => {
//   return Promise.reject(error);
// });

axiosHttp.interceptors.request.use(config => {
  const token = localStorage.getItem('repassa_auth')
  if (token) {
    config.headers.Authorization = ""+token
  }
  return config;
}, (error) => {
  return Promise.reject(error);
});

export default axiosHttp;