import axiosHttp from './axios';
import handleError from './handleError';

// HTTP GET Request - Returns Resolved or Rejected Promise
export const get = (path, data) => {
  return new Promise((resolve, reject) => {
    axiosHttp.get(`${path}`, data)
      .then(response => { resolve(response) })
      .catch(error => { reject(handleError(error)) });
  });
};
// HTTP PATCH Request - Returns Resolved or Rejected Promise
export const patch = (path, data) => {
  return new Promise((resolve, reject) => {
    axiosHttp.patch(`${path}`, data)
      .then(response => { resolve(response) })
      .catch(error => { reject(handleError(error)) });
  });
};
// HTTP POST Request - Returns Resolved or Rejected Promise
export const post = (path, data) => {
  return new Promise((resolve, reject) => {
    axiosHttp.post(`${path}`, data)
      .then(response => { resolve(response) })
      .catch(error => { reject(handleError(error)) });
  });
};
// HTTP DELETE Request - Returns Resolved or Rejected Promise
export const del = (path, data) => {
  return new Promise((resolve, reject) => {
    axiosHttp.delete(`${path}`, data)
      .then(response => { resolve(response) })
      .catch(error => { reject(handleError(error)) });
  });
};