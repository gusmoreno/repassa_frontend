export default (error) => {
  var status = error.response.status
  switch (status) {
    case 401:
      console.log('Erro 401')
      window.location.href = 'http://localhost:3000/api/login'
      // do something when you're unauthenticated
    case 403:
      console.log('Erro 403')
      console.log(error)
      // do something when you're unauthorized to access a resource
    case 500:
      console.log('Erro 500')
      console.log(error)
      // do something when your server exploded
    default:
      console.log('Erro default')
      console.log(error)
  }
  return error; // I like to get my error message back
}