import { get, post } from '../apiUtils'

export default {
    index(){
      return get("evaluations")
    },
    create(evaluation){
      return post("evaluations", { evaluation: evaluation })
    }
  }