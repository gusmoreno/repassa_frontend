import { get, post, patch, del } from '../apiUtils'

export default {
    index() {
      return get("users")
    },
    edit(id) {
      return get("users/"+id)
    },
    create(user) {
      return post("users", { user: user })
    },
    update(user) {
      return patch("users/"+user.id, { user: user })
    },
    delete(id) {
      return del("users/"+id)
    },
    managedUsers() {
      return get("users/managed")
    },
    managerOptions(id) {
      return get("users/" + id + "/manager_options")
    },
  }