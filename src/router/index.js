import Evaluations from "@/components/Evaluations";
import Users from "@/components/Users";
import Login from "@/components/Login";

export const routes = [
  { path: "/login", name: "Login", component: Login, meta: { requiresAuth: false} },
  { path: '/avaliacoes', name: 'Evaluations', component: Evaluations, meta: { requiresAuth: true} },
  { path: '/usuarios', name: 'Users', component: Users, meta: { requiresAuth: true} },
];
